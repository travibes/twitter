package mutation

import (
	"fmt"
	"math/rand"
	"time"

	"github.com/graphql-go/graphql"

	"schema"
)

var MutationType = graphql.NewObject(graphql.ObjectConfig{
	Name: "Mutation",
	Fields: graphql.Fields{
		//create new user/signup
		"signup": &graphql.Field{
			Type: graphql.NewList(schema.UserType),
			Description: "Create new user",
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Int),
				},
				"name": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
				"email": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
				"password": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
				//"post": &graphql.ArgumentConfig{
				//Type: graphql.NewList(schema.PostType),
				//	},
			},
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				fmt.Println("inside mutation resolve")
				rand.Seed(time.Now().UnixNano())
				user := schema.User{ID: int64(rand.Intn(100000)), Name: params.Args["name"].(string), Email: params.Args["email"].(string), Password: params.Args["password"].(string), Post: []schema.Post{schema.Post{ID:1, Title:"yes", Content:"haha"}}}
				fmt.Println(user)
				var users []schema.User
				users = append(users, user)
				fmt.Println(users)
				return users, nil
			},
		},
		"login": &graphql.Field{
			Type:        schema.UserType,
			Description: "User logged in",
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"name": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"email": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
				"password": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
				//"post": &graphql.ArgumentConfig{
				//	Type: graphql.NewList(schema.PostType),
				//},
			},
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				users := []schema.User{schema.User{ID: 1, Name: "prakash", Email: "prakashveer.nith@gmail.com", Password: "fwfwfb57757jbkfkbfkw", Post: []schema.Post{schema.Post{ID: 1, Title: "My biography", Content: "I am hero of myself"}}}, schema.User{ID: 2, Name: "raja", Email: "prakashveer15792@gmail.com", Password: "fwfwfb57757jbkfkbf34r", Post: []schema.Post{schema.Post{ID: 2, Title: "My biography", Content: "I am hero of myself"}}}}
				email, e := params.Args["email"].(string)
				password, p := params.Args["password"].(string)

				if e && p {
					//find email and password in users array
					for _, user := range users {
						if string(user.Email) == email && string(user.Password) == password {
							return user, nil
						}
					}
				}
				return nil, nil
			},
		},
		"posttweet": &graphql.Field{
			Type: schema.PostType,
			Description: "User will post a tweet",
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"title": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
				"content": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
			},
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				rand.Seed(time.Now().UnixNano())
				tweet := schema.Post{ID: int64(rand.Intn(100000)), Title:"Feeling Happy", Content:"I have been feeling very positive and full of energy since last 1 year. God bless me!!"}
				return tweet, nil
			},
		},
	},
})
