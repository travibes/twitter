package resolver

import (
	"schema"

	"github.com/graphql-go/graphql"
)

var QueryType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "Query",
		Fields: graphql.Fields{
			"postlist": &graphql.Field{
				Type:        schema.PostType,
				Description: "get post lists",
				Args: graphql.FieldConfigArgument{
					"id": &graphql.ArgumentConfig{
						Type: graphql.Int,
					},
				},
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					id, ok := p.Args["id"].(int)
					if ok {
						//Find post to corresponding ID
						posts := []schema.Post{schema.Post{ID: 1, Title: "Chicha Morada", Content: "Chicha morada is a beverage originated in the Andean regions of Perú but is actually consumed at a national level (wiki)"}, schema.Post{ID: 2, Title: "Chicha de jora", Content: "Chicha de jora is a corn beer chicha prepared by germinating maize, extracting the malt sugars, boiling the wort, and fermenting it in large vessels (traditionally huge earthenware vats) for several days (wiki)"}}
						for _, post := range posts {
							if int(post.ID) == id {
								return post, nil
							}
						}
					}
					return nil, nil
				},
			},
			"list": &graphql.Field{
				Type:        graphql.NewList(schema.PostType),
				Description: "Get posts list",
				Resolve: func(params graphql.ResolveParams) (interface{}, error) {
					posts := []schema.Post{schema.Post{ID: 1, Title: "Chicha Morada", Content: "Chicha morada is a beverage originated in the Andean regions of Perú but is actually consumed at a national level (wiki)"}, schema.Post{ID: 2, Title: "Chicha de jora", Content: "Chicha de jora is a corn beer chicha prepared by germinating maize, extracting the malt sugars, boiling the wort, and fermenting it in large vessels (traditionally huge earthenware vats) for several days (wiki)"}}
					return posts, nil
				},
			},
			"listpost": &graphql.Field{
				Type:        graphql.NewList(schema.PostType),
				Description: "List all post of a user",
				Args: graphql.FieldConfigArgument{
					"id": &graphql.ArgumentConfig{
						Type: graphql.Int,
					},
				},
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					//id, ok := p.Args["id"].(int64)
					//user := schema.User{ID: id, Name: "prakash", Email: "prakashveer.nith@gmail.com", Password: "fwfwfb57757jbkfkbfkw", Post: []schema.Post{schema.Post{ID: 1, Title: "Feeling Happy", Content: "I have been feeling very positive and full of energy since last 1 year. God bless me!!"}, schema.Post{ID: 2, Title: "The great democracy", Content: "Democracy is the most important pillar of any sovereign country!!"}}}
					posts := []schema.Post{schema.Post{ID: 1, Title: "Feeling Happy", Content: "I have been feeling very positive and full of energy since last 1 year. God bless me!!"}, schema.Post{ID: 2, Title: "The great democracy", Content: "Democracy is the most important pillar of any sovereign country!!"}}
					// ok {
					//find all posts corresponding to that userId
					return posts, nil
					//}
					//return nil, nil
				},
			},
			"otherusers": &graphql.Field{
				Type: graphql.NewList(schema.UserType),
				Description: "GET list of other users",
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					users := []schema.User{schema.User{ID: 1, Name: "prakash", Email: "prakashveer.nith@gmail.com", Password: "fwfwfb57757jbkfkbfkw", Post: []schema.Post{schema.Post{ID: 1, Title: "My biography", Content: "I am hero of myself"}}}, schema.User{ID: 2, Name: "raja", Email: "prakashveer15792@gmail.com", Password: "fwfwfb57757jbkfkbf34r", Post: []schema.Post{schema.Post{ID: 2, Title: "My biography", Content: "I am hero of myself"}}}}
					return users, nil
				},
			},
		},
	})
